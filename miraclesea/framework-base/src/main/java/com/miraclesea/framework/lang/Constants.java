package com.miraclesea.framework.lang;

public final class Constants {
	
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");
	
	private Constants() { }
}
