package com.miraclesea.component.log;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.junit.Test;
import org.mockito.Mock;

import com.miraclesea.framework.lang.Constants;
import com.miraclesea.test.integrate.MockBaseTest;

public final class ErrorTraceTest extends MockBaseTest {
	
	@Mock
	private ProceedingJoinPoint pjp;
	
	@Mock
	private Signature signature;
	
	@Test
	public void getExceptionInfo() {
		when(signature.toLongString()).thenReturn("public void com.miraclesea.component.log.ErrorTrace.xxx");
		when(pjp.getSignature()).thenReturn(signature);
		when(pjp.getArgs()).thenReturn(new Object[] {"C"});
		ErrorTrace errorTrace = new ErrorTrace(new RuntimeException("This is an exception."), pjp);
		String[] actuals = errorTrace.getExceptionInfo().split(Constants.LINE_SEPARATOR);
		assertThat(actuals.length, is(4));
		assertThat(actuals[0], is("Got an unexpected exception. It is: java.lang.RuntimeException: This is an exception."));
		assertThat(actuals[1], is("Replay the method call scenario..."));
		assertThat(actuals[2], is("Call: public void com.miraclesea.component.log.ErrorTrace.xxx"));
		assertThat(actuals[3], is("There are 1 parameter(s), value(s) are: 1 -> C"));
		verify(signature).toLongString();
		verify(pjp).getSignature();
		verify(pjp).getArgs();
	}
}
