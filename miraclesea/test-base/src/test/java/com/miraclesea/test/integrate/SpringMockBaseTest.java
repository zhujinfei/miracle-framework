package com.miraclesea.test.integrate;

import org.junit.Before;
import org.mockito.MockitoAnnotations;

import com.miraclesea.test.inject.InjectUtil;

public abstract class SpringMockBaseTest extends SpringBaseTest {
	
	@Before
	public final void init() throws IllegalAccessException {
		MockitoAnnotations.initMocks(this);
		InjectUtil.injectTargetForProxyBean(this);
	}
}
