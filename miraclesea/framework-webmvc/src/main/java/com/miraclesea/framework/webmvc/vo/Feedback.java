package com.miraclesea.framework.webmvc.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public final class Feedback {
	
	private final String code;
	private final String message;
}
